Source: crackmapexec
Section: misc
Priority: optional
Maintainer: Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>
Uploaders: Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3-all,
               python3-bs4,
               python3-msgpack,
               python3-pylnk3,
               python3-setuptools
Standards-Version: 4.5.0
Homepage: https://github.com/byt3bl33d3r/CrackMapExec

Package: crackmapexec
Architecture: all
Depends: python3-impacket,
         python3-pywerview,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Description: Swiss army knife for pentesting networks
 This package is a swiss army knife for pentesting Windows/Active Directory
 environments.
 .
 From enumerating logged on users and spidering SMB shares to executing psexec
 style attacks, auto-injecting Mimikatz/Shellcode/DLL's into memory using
 Powershell, dumping the NTDS.dit and more.
 .
 The biggest improvements over the above tools are:
  - Pure Python script, no external tools required
  - Fully concurrent threading
  - Uses **ONLY** native WinAPI calls for discovering sessions, users, dumping
    SAM hashes etc...
  - Opsec safe (no binaries are uploaded to dump clear-text credentials, inject
    shellcode etc...)
 .
 Additionally, a database is used to store used/dumped credentals. It also
 automatically correlates Admin credentials to hosts and vice-versa allowing you
 to easily keep track of credential sets and gain additional situational
 awareness in large environments.
